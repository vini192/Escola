from django.contrib import admin
from django.urls import path, include
from Shop.views import *


urlpatterns = [
    path('', home_screen_view, name='home'),
    path('register/', registration_view, name='register'),
    path('logout/', logout_view, name='logout'),
    path('login/', login_view, name='login'),
    path('account/', account_view, name='account'),
    path('create', pessoaCreate, name='pessoaCreate'),
    path('update/<int:id>', pessoaUpdate, name='pessoaUpdate'),
    path('delete/<int:id>', pessoaDelete, name='pessoaDelete'),

]
