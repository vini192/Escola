from django.shortcuts import render, redirect,  get_object_or_404
from django.contrib.auth import login, authenticate, logout
from Shop.forms import *
from Shop.models import Account


# Create your views here.
def home_screen_view(request):
    context = {}
    accounts = Account.objects.all()
    pessoas = Pessoa.objects.all()
    context['accounts'] = accounts
    return render(request, "base.html", {'accounts': accounts, 'pessoas': pessoas})


def registration_view(request):
    context = {}
    if request.POST:
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            email = form.cleaned_data.get('email')
            raw_password = form.cleaned_data.get('password1')
            account = authenticate(email=email, password=raw_password)
            login(request, account)
            return redirect('home')
        else:
            context['registration_form'] = form

    else:
        form = RegistrationForm()
        context['registration_form'] = form
    return render(request, 'account/register.html', context)


def logout_view(request):
    logout(request)
    return redirect('home')


def login_view(request):
    context = {}

    user = request.user
    if user.is_authenticated:
        return redirect("home")

    if request.POST:
        form = AccountAutheticationForm(request.POST)
        if form.is_valid():
            email = request.POST['email']
            password = request.POST['password']
            user = authenticate(email=email, password=password)

            if user:
                login(request, user)
                return redirect("home")

    else:
        form = AccountAutheticationForm()

    context['login_form'] = form

    # print(form)
    return render(request, "account/login.html", context)


def account_view(request):
    if not request.user.is_authenticated:
        return redirect("login")

    context = {}
    if request.POST:
        form = AccountUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('account')
    else:
        form = AccountUpdateForm(
            initial={
                "email": request.user.email,
                "username": request.user.username,
            }
        )

    context['account_form'] = form
    return render(request, "account/account.html", context)



def pessoaCreate(request):
    form = PessoaForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('home')

    return render(request, 'pessoaCreate.html', {'form': form})

def pessoaUpdate(request, id):
    pessoa = get_object_or_404(Pessoa, pk=id)
    form = PessoaForm(request.POST or None, instance=pessoa)
    if form.is_valid():
        form.save()
        return redirect('home')
    return render(request, 'pessoaCreate.html', {'form': form})

def pessoaDelete(request, id):
    pessoa = get_object_or_404(Pessoa, pk=id)
    form = PessoaForm(request.POST or None, instance=pessoa)
    if request.method == 'POST':
        pessoa.delete()
        return redirect('home')

    return render(request, 'pessoaDeletec.html', {'form':form})

